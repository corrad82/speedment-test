package com.company.fp_fbe;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings.AircraftBookingsManager;

public class Main
{
  public static void main(String[] args)
  {
    FpFbeApplication fpFbeApplication = new FpFbeApplicationBuilder()
      .withPassword("admin")
      .build();

    AircraftBookingsManager aircraftBookingsManager =
      fpFbeApplication.getOrThrow(AircraftBookingsManager.class);

    long count = aircraftBookingsManager.stream().count();

    System.out.println(count);

  }
}
