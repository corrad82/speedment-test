package com.company.fp_fbe;

import com.company.fp_fbe.generated.GeneratedFpFbeApplication;

/**
 * An {@link com.speedment.runtime.core.ApplicationBuilder} interface for the
 * {@link com.speedment.runtime.config.Project} named fp-fbe.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface FpFbeApplication extends GeneratedFpFbeApplication {
    
    
}