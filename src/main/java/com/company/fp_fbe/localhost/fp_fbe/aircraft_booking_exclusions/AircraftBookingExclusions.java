package com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions.generated.GeneratedAircraftBookingExclusions;

/**
 * The main interface for entities of the {@code
 * AIRCRAFT_BOOKING_EXCLUSIONS}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface AircraftBookingExclusions extends GeneratedAircraftBookingExclusions {
    
    
}