package com.company.fp_fbe.localhost.fp_fbe.fbe_settings;

import com.company.fp_fbe.localhost.fp_fbe.fbe_settings.generated.GeneratedFbeSettingsImpl;

/**
 * The default implementation of the {@link
 * com.company.fp_fbe.localhost.fp_fbe.fbe_settings.FbeSettings}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class FbeSettingsImpl extends GeneratedFbeSettingsImpl implements FbeSettings {
    
    
}