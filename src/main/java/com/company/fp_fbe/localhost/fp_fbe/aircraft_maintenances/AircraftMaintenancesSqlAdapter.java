package com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances.generated.GeneratedAircraftMaintenancesSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances.AircraftMaintenances}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class AircraftMaintenancesSqlAdapter extends GeneratedAircraftMaintenancesSqlAdapter {
    
    
}