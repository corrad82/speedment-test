package com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings.generated.GeneratedAircraftBookingsSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings.AircraftBookings}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class AircraftBookingsSqlAdapter extends GeneratedAircraftBookingsSqlAdapter {
    
    
}