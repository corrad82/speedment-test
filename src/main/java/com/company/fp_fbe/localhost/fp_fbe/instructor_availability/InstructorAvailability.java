package com.company.fp_fbe.localhost.fp_fbe.instructor_availability;

import com.company.fp_fbe.localhost.fp_fbe.instructor_availability.generated.GeneratedInstructorAvailability;

/**
 * The main interface for entities of the {@code INSTRUCTOR_AVAILABILITY}-table
 * in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface InstructorAvailability extends GeneratedInstructorAvailability {
    
    
}