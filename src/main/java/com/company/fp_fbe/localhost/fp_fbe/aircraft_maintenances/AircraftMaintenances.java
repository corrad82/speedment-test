package com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances.generated.GeneratedAircraftMaintenances;

/**
 * The main interface for entities of the {@code AIRCRAFT_MAINTENANCES}-table in
 * the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface AircraftMaintenances extends GeneratedAircraftMaintenances {
    
    
}