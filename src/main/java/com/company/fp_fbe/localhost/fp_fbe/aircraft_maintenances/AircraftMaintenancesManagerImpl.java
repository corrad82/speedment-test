package com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances.generated.GeneratedAircraftMaintenancesManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances.AircraftMaintenances}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class AircraftMaintenancesManagerImpl extends GeneratedAircraftMaintenancesManagerImpl implements AircraftMaintenancesManager {
    
    
}