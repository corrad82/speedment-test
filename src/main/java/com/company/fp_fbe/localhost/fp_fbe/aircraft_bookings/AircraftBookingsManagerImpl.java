package com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings.generated.GeneratedAircraftBookingsManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings.AircraftBookings}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class AircraftBookingsManagerImpl extends GeneratedAircraftBookingsManagerImpl implements AircraftBookingsManager {
    
    
}