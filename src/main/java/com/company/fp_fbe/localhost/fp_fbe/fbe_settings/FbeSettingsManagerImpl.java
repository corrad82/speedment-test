package com.company.fp_fbe.localhost.fp_fbe.fbe_settings;

import com.company.fp_fbe.localhost.fp_fbe.fbe_settings.generated.GeneratedFbeSettingsManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * com.company.fp_fbe.localhost.fp_fbe.fbe_settings.FbeSettings} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class FbeSettingsManagerImpl extends GeneratedFbeSettingsManagerImpl implements FbeSettingsManager {
    
    
}