package com.company.fp_fbe.localhost.fp_fbe.fbe_settings;

import com.company.fp_fbe.localhost.fp_fbe.fbe_settings.generated.GeneratedFbeSettingsManager;

/**
 * The main interface for the manager of every {@link
 * com.company.fp_fbe.localhost.fp_fbe.fbe_settings.FbeSettings} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface FbeSettingsManager extends GeneratedFbeSettingsManager {
    
    
}