package com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions.generated.GeneratedAircraftBookingExclusionsSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions.AircraftBookingExclusions}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class AircraftBookingExclusionsSqlAdapter extends GeneratedAircraftBookingExclusionsSqlAdapter {
    
    
}