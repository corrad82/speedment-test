package com.company.fp_fbe.localhost.fp_fbe.user_aircrafts;

import com.company.fp_fbe.localhost.fp_fbe.user_aircrafts.generated.GeneratedUserAircrafts;

/**
 * The main interface for entities of the {@code USER_AIRCRAFTS}-table in the
 * database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface UserAircrafts extends GeneratedUserAircrafts {
    
    
}