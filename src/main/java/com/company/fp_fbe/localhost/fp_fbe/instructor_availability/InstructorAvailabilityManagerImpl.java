package com.company.fp_fbe.localhost.fp_fbe.instructor_availability;

import com.company.fp_fbe.localhost.fp_fbe.instructor_availability.generated.GeneratedInstructorAvailabilityManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * com.company.fp_fbe.localhost.fp_fbe.instructor_availability.InstructorAvailability}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class InstructorAvailabilityManagerImpl extends GeneratedInstructorAvailabilityManagerImpl implements InstructorAvailabilityManager {
    
    
}