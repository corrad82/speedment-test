package com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions.generated.GeneratedAircraftBookingExclusionsManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions.AircraftBookingExclusions}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class AircraftBookingExclusionsManagerImpl extends GeneratedAircraftBookingExclusionsManagerImpl implements AircraftBookingExclusionsManager {
    
    
}