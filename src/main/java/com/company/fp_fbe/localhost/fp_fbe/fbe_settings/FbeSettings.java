package com.company.fp_fbe.localhost.fp_fbe.fbe_settings;

import com.company.fp_fbe.localhost.fp_fbe.fbe_settings.generated.GeneratedFbeSettings;

/**
 * The main interface for entities of the {@code FBE_SETTINGS}-table in the
 * database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface FbeSettings extends GeneratedFbeSettings {
    
    
}