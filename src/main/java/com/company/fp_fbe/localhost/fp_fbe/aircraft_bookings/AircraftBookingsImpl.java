package com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings.generated.GeneratedAircraftBookingsImpl;

/**
 * The default implementation of the {@link
 * com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings.AircraftBookings}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class AircraftBookingsImpl extends GeneratedAircraftBookingsImpl implements AircraftBookings {
    
    
}