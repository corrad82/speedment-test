package com.company.fp_fbe.localhost.fp_fbe.instructor_availability;

import com.company.fp_fbe.localhost.fp_fbe.instructor_availability.generated.GeneratedInstructorAvailabilityImpl;

/**
 * The default implementation of the {@link
 * com.company.fp_fbe.localhost.fp_fbe.instructor_availability.InstructorAvailability}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class InstructorAvailabilityImpl extends GeneratedInstructorAvailabilityImpl implements InstructorAvailability {
    
    
}