package com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_bookings.generated.GeneratedAircraftBookings;

/**
 * The main interface for entities of the {@code AIRCRAFT_BOOKINGS}-table in the
 * database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface AircraftBookings extends GeneratedAircraftBookings {
    
    
}