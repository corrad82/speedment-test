package com.company.fp_fbe.localhost.fp_fbe.fbe_settings;

import com.company.fp_fbe.localhost.fp_fbe.fbe_settings.generated.GeneratedFbeSettingsSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * com.company.fp_fbe.localhost.fp_fbe.fbe_settings.FbeSettings} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class FbeSettingsSqlAdapter extends GeneratedFbeSettingsSqlAdapter {
    
    
}