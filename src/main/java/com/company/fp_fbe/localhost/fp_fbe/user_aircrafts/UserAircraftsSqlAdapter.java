package com.company.fp_fbe.localhost.fp_fbe.user_aircrafts;

import com.company.fp_fbe.localhost.fp_fbe.user_aircrafts.generated.GeneratedUserAircraftsSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * com.company.fp_fbe.localhost.fp_fbe.user_aircrafts.UserAircrafts} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class UserAircraftsSqlAdapter extends GeneratedUserAircraftsSqlAdapter {
    
    
}