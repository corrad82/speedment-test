package com.company.fp_fbe.localhost.fp_fbe.user_aircrafts;

import com.company.fp_fbe.localhost.fp_fbe.user_aircrafts.generated.GeneratedUserAircraftsImpl;

/**
 * The default implementation of the {@link
 * com.company.fp_fbe.localhost.fp_fbe.user_aircrafts.UserAircrafts}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class UserAircraftsImpl extends GeneratedUserAircraftsImpl implements UserAircrafts {
    
    
}