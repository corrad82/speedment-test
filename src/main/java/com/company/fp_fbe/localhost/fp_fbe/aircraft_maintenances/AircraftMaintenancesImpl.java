package com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances.generated.GeneratedAircraftMaintenancesImpl;

/**
 * The default implementation of the {@link
 * com.company.fp_fbe.localhost.fp_fbe.aircraft_maintenances.AircraftMaintenances}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class AircraftMaintenancesImpl extends GeneratedAircraftMaintenancesImpl implements AircraftMaintenances {
    
    
}