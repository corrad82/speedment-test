package com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions;

import com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions.generated.GeneratedAircraftBookingExclusionsImpl;

/**
 * The default implementation of the {@link
 * com.company.fp_fbe.localhost.fp_fbe.aircraft_booking_exclusions.AircraftBookingExclusions}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class AircraftBookingExclusionsImpl extends GeneratedAircraftBookingExclusionsImpl implements AircraftBookingExclusions {
    
    
}